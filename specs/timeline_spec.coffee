chai = require 'chai'
chai.should()

{Event, Timeline} = require '../lib/timeline.coffee'

describe 'Timeline', ->

  subject = event1 = event2 = undefined

  beforeEach ->
    event1 = new Event '2015-01', '2015-06', 'Event 1 title', 'Event 1 description'
    event2 = new Event '2015-09', '2015-10', 'Event 2 title', 'Event 2 description'
    subject = new Timeline [event1, event2], 'months'

  describe '#events', ->
    it 'should be the input array sorted by the most recent', ->
      subject.events.should.deep.equal [event2, event1]

  describe '#first', ->
    it 'should be the most recent event', ->
      subject.first().should.equal event2

  describe '#last', ->
    it 'should be the oldest event', ->
      subject.last().should.equal event1

  describe '#start', ->
    it 'should be the end date of the most recent event', ->
      subject.start().toString().should.equal event2.end().toString()

  describe '#end()', ->
    it 'should be the start date of the oldest event', ->
      subject.end().toString().should.equal event1.start().toString()

  describe '#length()', ->
    it 'should be the difference (in months) between #from and #to', ->
      subject.length().should.equal 10

  describe '#y()', ->
    it 'should be the y position of the given event', ->
      subject.y(event2).should.equal 0
      subject.y(event1).should.equal 4

  describe '#h()', ->
    it 'should be the height of the given event', ->
      subject.h(event2).should.equal 2
      subject.h(event1).should.equal 6

  describe '#hasErrors', ->
    it 'should be true when all is hunky dory', ->
      subject.hasErrors.should.equal false
    it 'should be false if any event has errors', ->
      event1 = new Event '2015-02', '2015-01', 'Event 1 title', 'Event 1 description'
      subject = new Timeline [event1]
      subject.hasErrors.should.equal true
    it 'should define additional errors where events overlap', ->
      event1 = new Event '2015-04', '2015-10', 'Event 1 title', 'Event 1 description'
      event2 = new Event '2015-01', '2015-06', 'Event 2 title', 'Event 2 description'
      subject = new Timeline [event1, event2]
      expectedError = 'The "to" date overlaps with the "from" date of a more recent event'
      event2.errors.should.deep.equal [expectedError]
      subject.hasErrors.should.equal true

  describe '#wouldIntroduceConflict(event)', ->

    event = conflictingEvent = nonConflictingEvent = undefined

    beforeEach ->
      event               = new Event '2015-01', '2015-06'
      nonConflictingEvent = new Event '2015-07', '2015-08'
      conflictingEvent    = new Event '2015-05', '2015-06'
      subject = new Timeline [event1]

    it 'should reflect whether the passed event overlaps with it', ->
      event               = new Event '2015-01', '2015-06'
      nonConflictingEvent = new Event '2015-07', '2015-08'
      conflictingEvent    = new Event '2015-05', '2015-06'
      subject = new Timeline [event1]
      subject.wouldIntroduceConflict(conflictingEvent).should.equal true
      subject.wouldIntroduceConflict(nonConflictingEvent).should.equal false


  describe '#toObject', ->
    it 'should be a (json-friendly) object representation of the timeline data', ->
      subject.toObject().should.deep.equal
        events: [ event2.toObject(), event1.toObject() ]

  describe '.fromObject', ->
    it 'should be a timeline initialized from the passed object data (and hence support round tripping)', ->
      clone = Timeline.fromObject subject.toObject()
      clone.toObject().should.deep.equal subject.toObject()
