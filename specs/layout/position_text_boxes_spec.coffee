_ = require 'lodash'
chai = require 'chai'
chai.should()

{PositionTextBoxes} = require '../../lib/layout/position_text_boxes.coffee'

describe 'PositionTextBoxes', ->

  ctx = subject = undefined

  beforeEach ->

    ctx =
      blocks: [ { y:   0, h: 20, actualTextHeight: 10 }
                { y:  20, h: 30, actualTextHeight: 10 }
                { y:  50, h: 50, actualTextHeight: 10 } ]
      height: 100

    subject = new PositionTextBoxes

  describe 'skewiness (a measure of the quality of the text box positioning)', ->

    describe 'when all text boxes are perfectly aligned with the centre of their corresponding bar', ->

      beforeEach ->
        ctx.blocks[index].textY = value for value,index in [5, 30, 70]

      it 'should be zero for each block', ->
        subject.blockSkewiness(block).should.eq 0 for block in ctx.blocks

      it 'should be zero for the layout as a whole', ->
        subject.totalSkewiness(ctx).should.eq 0

    describe 'when some or all text boxes are not perfectly aligned with the centre of their corresponding bar', ->

      beforeEach ->
        ctx.blocks[index].textY = value for value,index in [0, 31, 80]

      it 'should reflect how far off center (in absolute terms) the alignment of the block is', ->
        subject.blockSkewiness(ctx.blocks[0]).should.eq 5
        subject.blockSkewiness(ctx.blocks[1]).should.eq 1
        subject.blockSkewiness(ctx.blocks[2]).should.eq 10

      it 'should be the sum of the skewiness of the individual blocks', ->
        subject.totalSkewiness(ctx).should.eq 16

  describe '#adjustDown (focusing only on the last block)', ->

    focus = undefined

    beforeEach ->
      ctx.blocks[index].textY = value for value,index in [0, 11, 21] # starting positions
      focus = ctx.blocks[2]

    describe 'when there is plenty of wiggle room', ->
      it 'should move the block down until reaches its more or less ideal location from a skewiness perspective', ->
        subject.adjustDown ctx, [focus]
        subject.blockSkewiness(focus).should.be.within -2, 2

    describe 'when there is not enough wiggle room', ->
      it 'should move the block down as far as it will go', ->
        focus.actualTextHeight = 60
        subject.adjustDown ctx, [focus]
        focus.textY.should.equal ctx.height - focus.actualTextHeight

    describe 'when there is zero wiggle room', ->
      it 'should leave the block where it is (immediately below the block above)', ->
        focus.actualTextHeight = 100
        subject.adjustDown ctx, [focus]
        focus.textY.should.equal ctx.blocks[1].textY + ctx.blocks[1].actualTextHeight

  describe '#adjustUp (focusing only on the first block)', ->

    focus = undefined

    beforeEach ->
      focus = ctx.blocks[0]

    describe 'when there is plenty of room to play with', ->

      beforeEach ->
        ctx.blocks[index].textY = value for value,index in [80, 90, 100]
        subject.adjustUp ctx

      it 'should move the block to its optimal position', ->
        focus.textY.should.equal 5

    describe 'when there is not enough room to play with', ->

      beforeEach ->
        focus.actualTextHeight = 40
        ctx.blocks[index].textY = value for value,index in [20, 90, 100]
        subject.adjustUp ctx

      it 'should move the block back up towards its optimal position as far as it will go', ->
        focus.textY.should.equal 0

    describe 'when the block is already at or above its optimal position', ->

      beforeEach ->
        ctx.blocks[index].textY = value for value,index in [4, 90, 100]
        subject.adjustUp ctx

      it 'should leave it where it is', ->
        focus.textY.should.equal 4


  describe '#layout', ->

    describe 'in simple cases', ->

      it 'should align all text boxes optimally', ->
        subject.layout ctx
        subject.totalSkewiness(ctx).should.equal 0

    describe 'in more complex cases', ->

      beforeEach ->
        ctx =
          blocks: [ { y:   0, h: 10, actualTextHeight:  5 }
                    { y:  10, h: 10, actualTextHeight: 30 }
                    { y:  20, h: 10, actualTextHeight: 30 }
                    { y:  30, h: 60, actualTextHeight: 30 }
                    { y:  90, h: 60, actualTextHeight: 35 } ]
          height: 150

        subject = new PositionTextBoxes
        subject.layout ctx

      it 'should do its best when things are hard', ->
        subject.totalSkewiness(ctx).should.be.lessThan 100



