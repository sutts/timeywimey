_ = require 'lodash'
chai = require 'chai'
chai.should()

{CalculateConnectorPositions} = require '../../lib/layout/calculate_connector_positions.coffee'

doLayout = (block) ->
  ctx =
    blocks: [block]
    connectorOffset: 2
  new CalculateConnectorPositions().layout ctx
  block

describe 'CalculateConnectorPositions', ->

  describe 'when the middle of the text block is level with any (non-edge) portion of its bar', ->

    it 'should define a straight line from the middle of the text title to the bar', ->
      focus = doLayout { textY: 100, actualTextHeight: 30, y: 100, h: 30 }
      focus.textConnector.should.equal 115
      focus.barConnector.should.equal 115

  describe 'when the middle of the text block is above its bar', ->
    it 'should define a diagonal line from the middle of the bar to the middle to the text block', ->
      focus = doLayout { textY: 80, actualTextHeight: 10, y: 100, h: 30 }
      focus.textConnector.should.equal 85
      focus.barConnector.should.equal 115

  describe 'when the middle of the text block is below its bar', ->
    it 'should define a diagonal line from the middle of the bar to the middle to the text block', ->
      focus = doLayout { textY: 140, actualTextHeight: 10, y: 100, h: 30 }
      focus.textConnector.should.equal 145
      focus.barConnector.should.equal 115

