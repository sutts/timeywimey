_ = require 'lodash'
chai = require 'chai'
chai.should()

{LayoutContext} = require '../../lib/layout/layout_context.coffee'

describe 'LayoutContext', ->

  event1 = event2 = timeline = undefined

  beforeEach ->
    event1 = {some: 'object1'}
    event2 = {some: 'object2'}
    timeline =
      events: [event1, event2]
      length: -> 50

  describe 'when the injected timeline is valid then', ->

    describe '#timeline', ->
      it 'should just be as injected', ->
        new LayoutContext(timeline, { height: 100 }).timeline.should.equal timeline

    describe '#height', ->
      it 'should just be as injected', ->
        new LayoutContext(timeline, { height: 100 }).height.should.equal 100

    describe '#gapHeight', ->
      it 'should just be as injected (or defaulted)', ->
        new LayoutContext(timeline, { height: 100 }).gapHeight.should.equal 0
        new LayoutContext(timeline, { height: 100, gapHeight: 10 }).gapHeight.should.equal 10

    describe '#effectiveHeight', ->

      describe 'when gap size is 0', ->
        it 'should just be the same as height', ->
          new LayoutContext(timeline, { height: 100 }).effectiveHeight.should.equal 100

      describe 'when gap size is > 0', ->
        it 'should be the height minus some number of gaps where that number is the number of events on the timeline plus 1', ->
          new LayoutContext(timeline, { height: 100, gapHeight: 10}).effectiveHeight.should.equal 70

    describe '#pixelsPerUnitOfTime', ->

      describe 'when timeline is not empty', ->
        it 'should be the effective height divided by the length of the timeline', ->
          new LayoutContext(timeline, { height: 100, gapHeight: 0 }).pixelsPerUnitOfTime.should.equal 2
          new LayoutContext(timeline, { height: 100, gapHeight: 10 }).pixelsPerUnitOfTime.should.equal 1.4

    describe '#blocks', ->
      it 'should wrap the underlying events', ->
        subject = new LayoutContext timeline, { height: 100 }
        _.pluck(subject.blocks, 'event').should.deep.equal [event1, event2]

  describe 'when the injected timeline is invalid then', ->

    it 'should complain vociferously', ->
      timeline.length = () -> 0
      (-> new LayoutContext timeline,   { height: 100 } ).should.throw 'Bad timeline'
      (-> new LayoutContext undefined,  { height: 100 } ).should.throw 'Bad timeline'
      (-> new LayoutContext {},         { height: 100 } ).should.throw 'Bad timeline'

