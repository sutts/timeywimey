_ = require 'lodash'
chai = require 'chai'
chai.should()

{PositionBarsNaively} = require '../../lib/layout/position_bars_naively.coffee'

describe 'PositionBarsNaively', ->

  event1 = event2 = timeline = ctx = undefined

  beforeEach ->

    event1 =
      y: 0
      h: 20

    event2 =
      y: 50
      h: 50

    timeline =
      events: [event1, event2]
      length: -> 100
      y: (event) -> event.y
      h: (event) -> event.h

    ctx =
      effectiveHeight: 200
      pixelsPerUnitOfTime: 2
      timeline: timeline
      blocks: timeline.events.map (event) -> {event: event}

    new PositionBarsNaively().layout ctx

  describe '#layout', ->

    it 'should set h (height) values for each block in a thoroughly linear and unsurprising way', ->
      _.pluck(ctx.blocks, 'h').should.deep.equal [40, 100]

    it 'should set y (height) values for each block in a thoroughly linear and unsurprising way', ->
      _.pluck(ctx.blocks, 'y').should.deep.equal [0, 100]
