chai = require 'chai'
chai.should()

{ExponentialScale} = require '../../lib/layout/scale_bars_globally.coffee'

describe 'ExponentialScale', ->

  subject = undefined

  describe 'with exponent value of 1', ->

    beforeEach ->
      subject = new ExponentialScale 100, 1

    it 'should do nothing interesting', ->
      subject.scale(i).should.equal(i) for i in [1..100]

  describe 'with exponent value greater than 1', ->

    beforeEach ->
      subject = new ExponentialScale 100, 2

    it 'should amplify higher values', ->
      subject.scale(0).should.equal 0
      subject.scale(10).should.equal 1
      subject.scale(25).should.equal 6.25
      subject.scale(50).should.equal 25
      subject.scale(75).should.equal 56.25
      subject.scale(90).should.equal 81
      subject.scale(100).should.equal 100

  describe 'with exponent value less than 1', ->

    beforeEach ->
      subject = new ExponentialScale 100, 0.5

    it 'should amplify lower values', ->
      subject.scale(0).should.equal 0
      subject.scale(10).should.be.within 31, 32
      subject.scale(25).should.equal 50
      subject.scale(50).should.be.within 70, 71
      subject.scale(75).should.be.within 86, 87
      subject.scale(90).should.be.within 94, 95
      subject.scale(100).should.equal 100
