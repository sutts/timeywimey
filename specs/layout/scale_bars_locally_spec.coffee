_ = require 'lodash'
chai = require 'chai'
chai.should()

{ScaleBarsLocally} = require '../../lib/layout/scale_bars_locally.coffee'

describe 'ScaleBarsLocally', ->

  ctx = undefined

  beforeEach ->
    block1 =
      y:  0
      h: 40
    block2 =
      y: 40
      h: 60
    block3 =
      y: 100
      h: 100
    ctx =
      effectiveHeight: 200
      blocks: [block1, block2, block3]

  describe 'when no events are locally scaled', ->

    beforeEach ->
      new ScaleBarsLocally().layout ctx

    it 'should have no effect on anything', ->
      _.pluck(ctx.blocks, 'h').should.deep.equal [40, 60, 100]

  describe 'when the first block is quasi doubled in scale', ->

    beforeEach ->
      ctx.blocks[0].event = { bias: 2 }
      new ScaleBarsLocally().layout ctx

    it 'should (in this case) increase the height of the first block (the one that is scaled)', ->
      ctx.blocks[0].h.should.be.within 66, 67

    it 'should (in this case) reduce the height of the other blocks (the ones that are not scaled)', ->
      ctx.blocks[1].h.should.equal 50
      ctx.blocks[2].h.should.be.within 83, 84

    it 'should adjust the y positions of the other blocks (the ones that are not scaled)', ->
      # Simplistic test in that this is a continuous timeline
      ctx.blocks[0].y.should.equal 0
      ctx.blocks[1].y.should.equal ctx.blocks[0].y + ctx.blocks[0].h
      ctx.blocks[2].y.should.equal ctx.blocks[1].y + ctx.blocks[1].h

    it 'should still produce a layout with a combined bar height of 200', ->
      (ctx.blocks[0].h + ctx.blocks[1].h + ctx.blocks[2].h).should.equal 200

  describe 'when the all blocks are scaled somewhat and the timeline includes gaps ', ->

    beforeEach ->
      block1 =
        y:  0
        h: 40
        bias: 0.9
      block2 =
        y: 140
        h: 60
        bias: 0.5
      block3 =
        y: 300
        h: 100
        bias: 1.5
      ctx =
        effectiveHeight: 400
        blocks: [block1, block2, block3]

      new ScaleBarsLocally().layout ctx

    it 'should still produce a layout with a combined bar height of 200', ->
      (ctx.blocks[0].h + ctx.blocks[1].h + ctx.blocks[2].h).should.equal 200

    it 'should adjust the y positions accordingly', ->
      (ctx.blocks[2].y + ctx.blocks[2].h).should.equal 400
