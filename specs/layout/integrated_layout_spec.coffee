_ = require 'lodash'
chai = require 'chai'
chai.should()

{LayoutContext} = require '../../lib/layout/layout_context.coffee'

describe 'LayoutContext (integrated)', ->

  event1 = event2 = event3 = timeline = subject = undefined

  beforeEach ->
    event1 =
      y: 0
      h: 20
    event2 =
      y: 20
      h: 30
    event3 =
      y: 50
      h: 50
    timeline =
      events: [event1, event2, event3]
      length: -> 100
      y: (event) -> event.y
      h: (event) -> event.h

  describe 'simplest case', ->

    beforeEach ->
      subject = new LayoutContext timeline, { height: 100 }
      subject.layout()

    it 'should sets pixels per unit of time to be 1 which will make this easier to test', ->
      subject.pixelsPerUnitOfTime.should.equal 1

    it 'should ultimately position all vertical bars to match their logical positions (because pixel size is 1 and no gaps or scaling', ->
      for block in subject.blocks
        block.y.should.equal block.event.y
        block.h.should.equal block.event.h

    it 'should position all text blocks to the center of their corresponding bar', ->
      _.pluck(subject.blocks, 'textY').should.deep.equal [10, 35, 75]
