_ = require 'lodash'
chai = require 'chai'
chai.should()

{InsertGapsBetweenBars} = require '../../lib/layout/insert_gaps_between_bars.coffee'

describe 'InsertGapsBetweenBars', ->

  ctx = undefined

  beforeEach ->
    ctx =
      blocks: [1..3].map (n) -> { y: n*100 }

  describe '#layout', ->

    describe 'when gap height is 0', ->

      beforeEach ->
        ctx.gapHeight = 0
        new InsertGapsBetweenBars().layout ctx

      it 'should have no impact on anything', ->
        _.pluck(ctx.blocks, 'y').should.deep.equal [100, 200, 300]

    describe 'when gap height is greater than zero', ->

      beforeEach ->
        ctx.gapHeight = 1
        new InsertGapsBetweenBars().layout ctx

      it 'should move all blocks down by a corresponding amount', ->
        _.pluck(ctx.blocks, 'y').should.deep.equal [101, 202, 303]
