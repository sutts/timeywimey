chai = require 'chai'
chaiString = require 'chai-string'
chai.should()
chai.use chaiString

{Event, Timeline} = require '../lib/timeline.coffee'

describe 'Event', ->

  subject = undefined

  beforeEach ->
    subject = new Event '2015-01', '2015-02', 'Event title', 'Event description'

  describe '#from', ->
    it 'should be the first day of the given month', ->
      subject.from.toString().should.startWith 'Thu Jan 01 2015'

  describe '#to', ->
    it 'should be the last day of the given month', ->
      subject.to.toString().should.startWith 'Sat Feb 28 2015'

  describe '#errors', ->
    it 'should be empty if the to date is same or later than the from date', ->
      new Event('2015-01', '2015-01').errors.should.deep.equal []
      new Event('2015-01', '2015-02').errors.should.deep.equal []
    it 'should be non-empty if the to date is earlier than the from date', ->
      new Event('2015-02', '2015-01').errors.should.deep.equal ['The "from" date must be less than or equal to the "to" date']

  describe '#overlapsWith()', ->
    beforeEach ->
      subject = new Event '2015-01', '2015-06'

    it 'should reflect whether there are any overlaps', ->
      subject.overlapsWith(new Event '2014-12', '2014-12').should.equal false
      subject.overlapsWith(new Event '2014-12', '2015-01').should.equal true
      subject.overlapsWith(new Event '2015-07', '2015-08').should.equal false
      subject.overlapsWith(new Event '2015-05', '2015-08').should.equal true
      subject.overlapsWith(new Event '2014-01', '2016-12').should.equal true

  describe '#length()', ->
    it 'should be the number of - units of time - between the start and the end', ->
      event1 = new Event '2015-01', '2015-01'
      event2 = new Event '2015-01', '2015-02'
      event1.length('months').should.equal 1
      event2.length('months').should.equal 2
      event1.length('days').should.equal 31
      event2.length('days').should.equal 59

  describe '#toObject', ->
    it 'should be a (json-friendly) object representation of the event data', ->
      subject.toObject().should.deep.equal
        from: '2015-01'
        to: '2015-02'
        title: 'Event title'
        description: 'Event description'
        bias: 1
    it 'should support additional optional stuff', ->
      subject.bias = 0.5
      subject.toObject().should.deep.equal
        from: '2015-01'
        to: '2015-02'
        title: 'Event title'
        description: 'Event description'
        bias: 0.5

  describe '.fromObject', ->
    it 'should be an event initialized from the passed object data (and hence support round tripping)', ->
      clone = Event.fromObject subject.toObject()
      clone.toObject().should.deep.equal subject.toObject()

