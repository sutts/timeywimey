window.safeTimeline= {
  "events": [
    {
      "from": "2014-08",
      "to": "2016-02",
      "title": "Yoga teacher (self-employed)",
      "description": "Teaching community drop-in sessions as well as courses in yoga and meditation, health clubs, gyms, corporations and schools. Having run 400  sessions by end of Feb 16"
    },
    {
      "from": "2014-06",
      "to": "2014-07",
      "title": "Diploma of Holistic Meditation",
      "description": "Meditation teacher Diploma, Yoga Alliance Australia"
    },
    {
      "from": "2014-04",
      "to": "2014-05",
      "title": "Yoga teacher training",
      "description": "Sivananda Yoga Vedanta Centre, Netala, India"
    },
    {
      "from": "2012-11",
      "to": "2014-03",
      "title": "Project Management Trainer",
      "description": "Development of online courses for Diploma and Certificate IV in Project Management, student support and assessment"
    },
    {
      "from": "2012-03",
      "to": "2012-08",
      "title": "Yoga studies, homeschooling",
      "description": "Bangalore, India"
    },
    {
      "from": "2012-01",
      "to": "2012-02",
      "title": "Volunteer Tutor with Read Write Now"
    },
    {
      "from": "2011-09",
      "to": "2011-12",
      "title": "Project Management Course Developer",
      "description": "Creation of complete courseware for project management courses, supporting project management students"
    },
    {
      "from": "2011-06",
      "to": "2011-08",
      "title": "Assessor",
      "description": "Assessing and supporting project management students"
    },
    {
      "from": "2010-09",
      "to": "2010-12",
      "title": "Cert IV in Training and Assessment",
      "description": "Australian College of Training and Employment"
    },
    {
      "from": "2000-01",
      "to": "2010-07",
      "title": "Homemaker raising 3 kids",
      "description": "Waterford, Melbourne, Perth. Part time work as freelance translator, web marketer\nCertificate in Practical Teaching Skills for Adult Education Tutors",
      "bias": 0.1
    },
    {
      "from": "1999-01",
      "to": "1999-12",
      "title": "Project Manager, Microsoft, Dublin",
      "description": "Design and implementation of new interactive controlling tool for project managers and providers"
    },
    {
      "from": "1998-07",
      "to": "1998-12",
      "title": "Technical Project Manager, Tek Trans, Dublin",
      "description": "Implementation of localisation tools and MS Office automation for localisation process, project management"
    },
    {
      "from": "1994-12",
      "to": "1998-05",
      "title": "Project Manager, Bayer, Leverkusen",
      "description": "Project management and design of intranet services, consulting on internet presentations"
    },
    {
      "from": "1991-10",
      "to": "1994-05",
      "title": "PhD Natural Sciences, Marburg"
    },
    {
      "from": "1986-10",
      "to": "1991-05",
      "title": "Bsc Physical Chemistry"
    }
  ]
};