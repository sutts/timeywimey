_ = require 'lodash'
Moment = require 'moment'

class Event

  constructor: (from, to, @title, @description) ->
    @from = @start(from).toDate()
    @to = @end(to).toDate()
    @bias = 1
    @refresh()

  refresh: ->
    @errors = []
    @errors.push 'The "from" date must be less than or equal to the "to" date' if @start().isAfter @end()

  start: (date = @from) -> new Moment(date).startOf 'month'
  end:   (date = @to)   -> new Moment(date).endOf 'month'

  length: (unitOfTime='days') -> 1 + @end().diff @start(), unitOfTime

  overlapsWith: (event, bounce=true) ->
    containedbyMe = event.start().isBetween(@start(), @end()) or event.end().isBetween(@start(), @end())
    containsMe = if bounce then event.overlapsWith(this, false) else false
    containedbyMe or containsMe

  toObject: ->
    obj =
      from: @start().format 'YYYY-MM'
      to: @end().format 'YYYY-MM'
      title: @title
      description: @description
    obj.bias = @bias
    obj

  @fromObject: (obj) ->
    event = new Event obj.from, obj.to, obj.title, obj.description
    event.bias = obj.bias if obj.bias?
    event


class Timeline

  constructor: (@events, @unitOfTime='days') ->
    @refresh()

  refresh: =>
    @events = @events.sort (a,b) -> b.to.getTime() - a.to.getTime()
    for event,index in @events
      event.refresh()
      event.errors.push 'The "to" date overlaps with the "from" date of a more recent event' if index > 0 and event.overlapsWith @events[index-1]
    @hasErrors = _.flatten(_.pluck(@events, 'errors')).length > 0

  first:  -> @events[0]
  last:   -> @events[@events.length-1]
  start:  -> @first().end()
  end:    -> @last().start()
  length: -> 1 + @start().diff @end(), @unitOfTime

  y: (event) -> @start().diff event.end(), @unitOfTime
  h: (event) -> event.length @unitOfTime

  wouldIntroduceConflict: (newEvent) ->
    _.some @events, (event) -> newEvent.overlapsWith(event)

  toObject: ->
    events: @events.map (event) -> event.toObject()

  @fromObject: (obj) ->
    events = obj.events.map (eventObj) -> Event.fromObject eventObj
    new Timeline events


module.exports = { Event: Event, Timeline: Timeline}

