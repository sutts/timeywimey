module.exports.CalculateConnectorPositions =

  class CalculateConnectorPositions

    layout: (ctx) ->
      offset = ctx.connectorOffset or 2
      for block in ctx.blocks
        block.textConnector = block.textY + (block.actualTextHeight / 2)
        min = block.y + offset
        max = block.y + block.h - offset
        block.barConnector =
          if block.textConnector > min and block.textConnector < max then block.textConnector
          else block.y + (block.h / 2)



