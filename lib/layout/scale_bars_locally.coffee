_ = require 'lodash'

module.exports.ScaleBarsLocally =

  class ScaleBarsLocally

    layout: (ctx) ->
      @calculateNaivelyScaledHeights ctx
      @calculateAdjustScaledHeightsToFit ctx
      @applyRevisedValuesForY ctx
      @applyRevisedValuesForHeightAndCleanUp ctx

    calculateNaivelyScaledHeights: (ctx) ->
      block.scaledHeight = block.h * (block.event?.bias or 1) for block in ctx.blocks

    calculateAdjustScaledHeightsToFit: (ctx) ->
      sum = (attr) -> (runningTotal, block) -> runningTotal += block[attr]
      sumOfScaledHeights = _.reduce ctx.blocks, sum('scaledHeight'), 0
      sumOfUnscaledHeights = _.reduce ctx.blocks, sum('h'), 0
      block.scaledHeight *= (sumOfUnscaledHeights / sumOfScaledHeights) for block in ctx.blocks

    applyRevisedValuesForY: (ctx) ->
      moveLowerBlocksUpOrDown = (ctx, index, delta) -> ctx.blocks[i].y += delta for i in [index...ctx.blocks.length]
      moveLowerBlocksUpOrDown ctx, index+1, block.scaledHeight - block.h for block,index in ctx.blocks

    applyRevisedValuesForHeightAndCleanUp: (ctx) ->
      for block in ctx.blocks
        block.h = block.scaledHeight
        delete block.scaledHeight




