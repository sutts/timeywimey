{PositionBarsNaively} = require './position_bars_naively.coffee'
{ScaleBarsLocally} = require './scale_bars_locally.coffee'
{ExponentialScale,ScaleBarsGlobally} = require './scale_bars_globally.coffee'
{InsertGapsBetweenBars} = require './insert_gaps_between_bars.coffee'
{PositionTextBoxes} = require './position_text_boxes.coffee'
{CalculateConnectorPositions} = require './calculate_connector_positions.coffee'

module.exports.LayoutContext =

  class LayoutContext

    constructor: (@timeline, @settings) ->
      throw 'Bad timeline' unless @timeline? and @timeline.events? and @timeline.length() > 0
      @height = @settings.height or 700
      @gapHeight = @settings.gapHeight or 0
      @effectiveHeight = @height - (@gapHeight * (@timeline.events.length + 1))
      @pixelsPerUnitOfTime = @effectiveHeight / @timeline.length()
      @refresh false

    refresh: (doLayout=true) ->
      @blocks = @timeline.events.map (event) => { event: event }
      @layout() if doLayout

    layout: ->
      new PositionBarsNaively().layout @
      new ScaleBarsLocally().layout @
      new ScaleBarsGlobally().layout @
      new InsertGapsBetweenBars().layout @
      new PositionTextBoxes().layout @
      new CalculateConnectorPositions().layout @
