module.exports.ExponentialScale =

  class ExponentialScale
    constructor: (@max, @exp=1) ->
    scale: (n) -> if @exp is 1 then n else (Math.pow(n, @exp) / Math.pow(@max, @exp)) * @max

module.exports.ScaleBarsGlobally =

  class ScaleBarsGlobally

    layout: (ctx) ->
      scaler = new ExponentialScale ctx.effectiveHeight, ctx.settings?.exponentialScale or 1
      for block in ctx.blocks
        y = block.y
        h = block.h
        block.y = scaler.scale y
        block.h = (scaler.scale y + h) - block.y



