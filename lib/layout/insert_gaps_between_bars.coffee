module.exports.InsertGapsBetweenBars =

  class InsertGapsBetweenBars

    layout: (ctx) ->
      for block,index in ctx.blocks
        block.y += (ctx.gapHeight * (index + 1))



