_ = require 'lodash'

barMiddle = (block) -> block.y + (block.h / 2)
textMiddle = (block) -> block.textY + (block.actualTextHeight / 2)
textBottom = (block) -> block.textY + block.actualTextHeight

module.exports.PositionTextBoxes =

  class PositionTextBoxes

    layout: (ctx) ->
      @positionBlocksOneAfterTheOther ctx
      @adjustDown ctx, ctx.blocks
      @adjustUp ctx

    totalSkewiness: (ctx) ->
      sum = (runningTotal, block) => runningTotal += @blockSkewiness(block)
      _.reduce ctx.blocks, sum, 0


    blockSkewiness: (block) ->
      Math.abs barMiddle(block) - textMiddle(block)

    positionBlocksOneAfterTheOther: (ctx) ->
      for block,index in ctx.blocks
        block.actualTextHeight = block.event?.actualTextHeight || 0 unless block.actualTextHeight?
        block.textY = if index is 0 then 0 else ctx.blocks[index-1].textY + ctx.blocks[index-1].actualTextHeight + 1

    adjustDown: (ctx, blocks) ->
      if blocks.length > 0
        amountToMove = 1
        if @spaceBelowLastBlock(ctx) >= amountToMove
          skewinessBefore = @totalSkewiness ctx
          block.textY += amountToMove for block in blocks
          skewinessAfter = @totalSkewiness ctx
          if skewinessAfter < skewinessBefore
            # recurse on this block
            @adjustDown ctx, blocks
          else
            # revert last move
            block.textY -= amountToMove for block in blocks
            # recurse on next block
            @adjustDown ctx, _.tail(blocks)

    adjustUp: (ctx) ->
      for block,index in ctx.blocks
        overskew = textMiddle(block) - barMiddle(block)
        if overskew > 0
          available = if index is 0 then block.textY else  block.textY - textBottom(ctx.blocks[index-1])
#          console.log " Index: #{index}, Bar middle #{barMiddle(block)}, Text middle #{textMiddle(block)} skew #{overskew} available #{available}"
          if available > 0
            block.textY -= Math.min overskew, available

    spaceBelowLastBlock: (ctx) =>
      bottom = _.last ctx.blocks
      ctx.height - (bottom.textY + bottom.actualTextHeight)
