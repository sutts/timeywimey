module.exports.PositionBarsNaively =

  class PositionBarsNaively

    layout: (ctx) ->
      for block in ctx.blocks
        block.y = ctx.pixelsPerUnitOfTime * ctx.timeline.y(block.event)
        block.h = ctx.pixelsPerUnitOfTime * ctx.timeline.h(block.event)



