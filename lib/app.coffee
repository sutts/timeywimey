Moment = require 'moment'

{Event,Timeline} = require './timeline.coffee'
{LayoutContext} = require './layout/layout_context.coffee'

window.app = angular.module 'timeywimey', ['ngAnimate', 'ui.bootstrap']

app.filter 'datestring', -> (date, format='MMM YYYY') -> if date then Moment(date).format(format) else '(empty)'

app.controller 'TWController', ($scope, $timeout) ->
  saved = localStorage.getItem 'timeline'
  if true and saved?
    obj = JSON.parse saved
    obj = window.safeTimeline
    $scope.timeline = Timeline.fromObject obj
  else
    event = new Event Moment().subtract(1, 'year'), Moment(), 'Example event' , 'Choose this event on the left to edit title, description and dates'
    $scope.timeline = new Timeline [event]

  $scope.settings =
    height: 750
    timeBias: 0
    gapHeight: 3

  createNewEventInScope = =>
    start = $scope.timeline.end().subtract 2, 'months'
    end = $scope.timeline.end().subtract 1, 'months'
    $scope.newEvent = new Event start, end

  createNewEventInScope()

  $scope.addEvent = =>
    event = $scope.newEvent
    form = event.form
    form[attr].$setTouched() for attr in ['from', 'to', 'title']
    if form.$valid
      $scope.timeline.events.push event
      $scope.timeline.refresh()
      createNewEventInScope().form = form
      form.$setUntouched()

  $scope.removeEvent = (event) =>
    $scope.timeline.events.splice $scope.timeline.events.indexOf(event)

  $scope.save = =>
    json = JSON.stringify $scope.timeline.toObject()
    localStorage.setItem 'timeline', json
    $scope.saveMessage = 'Changes saved'
    console.log 'saved!'

  changed = =>
    console.log 'changed'
    $scope.saveMessage = undefined
    $scope.timeline.refresh()
    unless $scope.timeline.hasErrors
      $scope.settings.exponentialScale = 1 + parseFloat($scope.settings.timeBias)
      ctx = new LayoutContext $scope.timeline, $scope.settings
      ctx.layout()
      $scope.layout = ctx

  $scope.$watch 'timeline', changed, true
  $scope.$watch 'settings', changed, true

  changed()

app.directive 'watchHeights', ['$timeout', ($timeout) ->
  restrict: 'A'
  scope: true
  link: ($scope, elem, attrs) ->
    attr = attrs['watchHeights']
    setViewHeightsInModel = ->
      for child,index in elem[0].children
#        console.log "Height of #{attr} for block #{index} is #{child.children[0].offsetHeight}"
        block = $scope.layout.blocks[index]
        block.event["actual#{attr}Height"] = child.children[0].offsetHeight
      $scope.layout.refresh()
    $timeout setViewHeightsInModel, 200
]

app.directive 'eventForm', [ ->
  scope:
    event: '='
  transclude: true
  templateUrl: '_event_form.html'
  link: ($scope, elem, attrs) ->
    $scope.event.form = $scope.form

]
